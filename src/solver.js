import { WORDS } from './words.js';

function numify(n) {
  if (n == 1)
    return 'first';
  else if (n == 2)
    return 'second';
  else if (n == 3)
    return 'third';
  else if (n == 4)
    return 'fourth';
  else if (n == 5)
    return 'fifth';
}

function colorify(c) {
  if (c == '-')
    return 'gray';
  else if (c == 'y')
    return 'yellow';
  else if (c == 'g')
    return 'green';
}

export class Solver {
  constructor() {
    this.atMost = new Map();
    this.atLeast = new Map();
    this.mask = [new Map(), new Map(), new Map(), new Map(), new Map()];
    this.keep = [undefined, undefined, undefined, undefined, undefined];
  }

  solved() {
    return this.keep.every(v => v != undefined);
  }

  clone() {
    // explicit 
    let result = new Solver();
    result.atMost = new Map(this.atMost);
    result.atLeast = new Map(this.atLeast);
    result.mask = [new Map(this.mask[0]), new Map(this.mask[1]), new Map(this.mask[2]), new Map(this.mask[3]), new Map(this.mask[4])];
    result.keep = [...this.keep];
    return result;
  }

  feed(word, state) {
    if (word.length != 5 || state.length != 5)
      throw `invalid argument passed to Solver::feed (${word}) (${state})`;

    let nInWord = new Map();
    let nInSoln = new Map();
    
    for (let i = 0; i < 5; ++i) {
      let w = word.charAt(i);
      let s = state.charAt(i);

      if (s == 'g') {
        if (this.keep[i] != undefined && this.keep[i] != w)
          throw `Conflicting input for the ${numify(i)} letter. ${w} and ${this.keep[i]} are both green!`
        this.keep[i] = w;
      } else if (this.keep[i] != undefined && this.keep[i] == w) {
        throw `Conflicting input for the ${numify(i)} letter. ${w} was green, but later it was marked as ${colorify(s)}!`
      }
      
      if (s == '-' || s == 'y') {
        this.mask[i].set(w, true);
      }

      nInWord.set(w, 1 + (nInWord.get(w) ? nInWord.get(w) : 0));

      nInSoln.set(w, ((s == 'y' || s == 'g') ? 1 : 0) + (nInSoln.get(w) ? nInSoln.get(w) : 0));
    }

    for (const entry of nInWord.entries()) {
      let [letter, _] = entry;
      if (nInWord.get(letter) > nInSoln.get(letter))
        this.atMost.set(letter, nInSoln.get(letter));
      if (nInSoln.get(letter) > 0 && (this.atLeast.get(letter) == undefined || nInSoln.get(letter) > this.atLeast.get(letter)))
        this.atLeast.set(letter, nInSoln.get(letter));
    }
  }

  match(word) {
    if (typeof(word) != 'string')
      return false;

    if (word.length != 5)
      return false;

    // console.log('word:', word);
    for (let i = 0; i < 5; ++i) {
      let letter = word.charAt(i);

      // console.log('mask:', i, this.mask[i][letter]);
      if (this.mask[i].get(letter))
        return false;

      // console.log('keep:', i, letter, this.keep[i]);
      if (this.keep[i] != undefined && this.keep[i] != letter)
        return false;
    }

    for (const entry of this.atMost) {
      let [letter, n] = entry;
      let count = word.split(letter).length - 1;
      if (count > n) {
        return false;
      }
    }

    for (const entry of this.atLeast) {
      let [letter, n] = entry;
      let count = word.split(letter).length - 1;
      if (count < n) {
        return false;
      }
    }

    // console.log('MATCH', word);
    return true;
  }

  simulate(word, solution) {
    let result = ["-", "-", "-", "-", "-"];
    const solutionArr = solution.split('');
    let yellowCount = new Map();
 
    for (let i = 0; i < 5; ++i) {
      const c = word.charAt(i);
      const n = solutionArr.map(s => s == c ? 1 : 0).reduce((a, b) => a + b, 0);

      if (c == solution.charAt(i)) {
        result[i] = "g";
        if (yellowCount.get(c) == undefined)
          yellowCount.set(c, 1);
        else
          yellowCount.set(c, yellowCount.get(c) + 1);

      }
    }
 
    for (let i = 0; i < 5; ++i) {
      const c = word.charAt(i);
      const n = solutionArr.map(s => s == c ? 1 : 0).reduce((a, b) => a + b, 0);

      if (solutionArr.includes(c) && !(yellowCount.get(c) >= n) && result[i] == "-") {
        result[i] = "y";
        if (yellowCount.get(c) == undefined)
          yellowCount.set(c, 1);
        else
          yellowCount.set(c, yellowCount.get(c) + 1);
      }
    }

    result = result.join("");

    this.feed(word, result);

    return result;
  }

  allMatchingWords(givenWords = WORDS) {
    let result = [];
    for (let i = 0; i < givenWords.length; ++i) {
      if (this.match(givenWords[i].word)) {
        // console.log('match!', WORDS[i]);
        result.push(givenWords[i]);
      }
    }

    // result = result.sort((a, b) => b.frequency - a.frequency);

    return result;
  }

  bestWords6() {
    const A = "A".charCodeAt(0);
    // const allMatches = [...WORDS];
    const allMatches = this.allMatchingWords();
    const posStrengths = [new Map(), new Map(), new Map(), new Map(), new Map()];
    const countStrengths = new Array(26);
    for (let i = 0; i < 26; ++i)
      countStrengths[i] = new Map();

    const fweight = f => -1 / Math.log2(f);

    const allMatchesWithCounts = allMatches.map(mat => {
      const { word, frequency, inWordle } = mat;
      const letters = new Map();

      [...word].forEach(l => {
        letters.set(l, [...word].map(c => c == l ? 1 : 0).reduce((a, b) => a + b, 0))
      });

      for (let i = 0; i < 5; ++i) {
        const l = word.charAt(i);
        const cur = posStrengths[i].get(l);
        if (cur == undefined) {
          posStrengths[i].set(l, fweight(frequency));
        } else {
          posStrengths[i].set(l, cur + fweight(frequency));
        }
      }

      letters.forEach((count, letter) => {
        if (countStrengths[letter.charCodeAt(0) - A].get(count) == undefined)
          countStrengths[letter.charCodeAt(0) - A].set(count, fweight(frequency));
        else
          countStrengths[letter.charCodeAt(0) - A].set(count, countStrengths[letter.charCodeAt(0) - A].get(count) + fweight(frequency));
      })

      return { word, frequency, letters, inWordle }
    });


    
    let maxStrength1 = -Infinity;
    let maxStrength2 = -Infinity;

    let result = allMatchesWithCounts.map(mat => {
      const { word, frequency, letters, inWordle } = mat;

      let strength1 = 0.0;
      let strength2 = 0.0;
      
      letters.forEach((count, letter) => {
        strength1 += countStrengths[letter.charCodeAt(0) - A].get(count);
      });

      for (let i = 0; i < 5; ++i) {
        strength2 += posStrengths[i].get(word.charAt(i));
      }

      if (maxStrength1 < strength1)
        maxStrength1 = strength1;

      if (maxStrength2 < strength2)
        maxStrength2 = strength2;

      return { word, frequency, strength1, strength2, inWordle };
    });

    result = result.map(mat => {
      const { word, frequency, strength1, strength2, inWordle } = mat;
      // console.log(word, strength1 / maxStrength1, strength2 / maxStrength2);
      var strength = (strength1 / maxStrength1) + (strength2 / maxStrength2);
      if (inWordle)
        strength = strength * 1.25;
      return { word, frequency, strength };
    })

    result.sort((a, b) => b.strength - a.strength); // 4.7225 / 0.0325

    return result;
  }
}
